// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALPROJECT3_UnrealProject3GameModeBase_generated_h
#error "UnrealProject3GameModeBase.generated.h already included, missing '#pragma once' in UnrealProject3GameModeBase.h"
#endif
#define UNREALPROJECT3_UnrealProject3GameModeBase_generated_h

#define UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_RPC_WRAPPERS
#define UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUnrealProject3GameModeBase(); \
	friend struct Z_Construct_UClass_AUnrealProject3GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AUnrealProject3GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/UnrealProject3"), NO_API) \
	DECLARE_SERIALIZER(AUnrealProject3GameModeBase)


#define UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAUnrealProject3GameModeBase(); \
	friend struct Z_Construct_UClass_AUnrealProject3GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AUnrealProject3GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/UnrealProject3"), NO_API) \
	DECLARE_SERIALIZER(AUnrealProject3GameModeBase)


#define UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnrealProject3GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnrealProject3GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnrealProject3GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealProject3GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnrealProject3GameModeBase(AUnrealProject3GameModeBase&&); \
	NO_API AUnrealProject3GameModeBase(const AUnrealProject3GameModeBase&); \
public:


#define UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnrealProject3GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnrealProject3GameModeBase(AUnrealProject3GameModeBase&&); \
	NO_API AUnrealProject3GameModeBase(const AUnrealProject3GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnrealProject3GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealProject3GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnrealProject3GameModeBase)


#define UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_12_PROLOG
#define UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_RPC_WRAPPERS \
	UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_INCLASS \
	UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALPROJECT3_API UClass* StaticClass<class AUnrealProject3GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealProject3_Source_UnrealProject3_UnrealProject3GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
