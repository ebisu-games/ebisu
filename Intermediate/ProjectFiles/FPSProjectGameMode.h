#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealProject3GameModeBase.generated.h"


UCLASS()
class UNREALPROJECT3_API AFPSProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

	virtual void StartPlay() override;
}; #pragma once
